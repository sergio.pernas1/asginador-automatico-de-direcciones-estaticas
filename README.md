# Asginador automatico de direcciones estaticas



### Script y funcionamiento

Fichero `ip_assigment.sh`.

```sh
#!/bin/bash


# establecer el pool de direcciones
host_id_pool=(100 101 102)

# establecer la parte de red la direccion ip completa 
ip_network=192.168.0

# establecer la mascara de red
netmask=255.255.255.0

# establecer el gateway de red
gateway=192.168.0.1

# Creamos un bucle que recorre la lista, la variable 'host_id' asumira
# el valor de cada elemento de la lista en cada vuelta.

for host_id in "${host_id_pool[@]}";do

	# En cada vuelta la variable 'ip_to_asign' guarda la direccion IP completa (ip_network + host_id)

	ip_to_assing="${ip_network}.${host_id}"

	# en cada vuelta se hace un ping a la direccion IP que es
	# el valor que tenga '$ip_to_assing'.
	
	if ! ping -c 1 $ip_to_assing > /dev/null 2>&1; then
		
		# Si el ping no responde entonces significa que la ip esta libre
		# y se puede asignar al host, para ello se sobre escribe el fichero
		# de configuraicon '/etc/network/interfaces' (Debian).

		echo "source /etc/network/interfaces.d/*
auto lo
iface lo inet loopback

allow-hotplug enp0s3
iface enp0s3 inet static
	address $ip_to_assing 
	netmask	$netmask
	gateway	$gateway" > /etc/network/interfaces


	# se reinica el servicio de red del sistema operativo para aplicar los cambios.

	systemctl restart networking.service

	# una vez hecho se sale del programa.
	
	exit
	
	fi	

done
```



Este script en Bash esta pensado para configurar la interfaz de red en una máquina Debian con direcciones IP estáticas. Aquí está una explicación paso a paso de lo que hace:

1. Se definen algunas variables:
   - `host_id_pool`: Un array que contiene tres números (100, 101 y 102). Estos números se utilizarán para asignar las últimas cifras de la dirección IP.
   - `ip_network`: Una cadena que representa la parte inicial de la dirección IP (192.168.0).
   - `netmask`: Una cadena que representa la máscara de subred (255.255.255.0).
   - `gateway`: La dirección IP del gateway predeterminado (192.168.0.1).

2. Luego, se inicia un bucle `for` que recorre los valores del array `host_id_pool`.

3. En cada iteración del bucle, se construye una dirección IP completa (`ip_to_assing`) concatenando la parte inicial de la dirección IP (`ip_network`) con el valor del `host_id` actual del array.

4. Se realiza una comprobación de ping para verificar si la dirección IP `ip_to_assing` ya está en uso. Si el ping no tiene éxito (es decir, si la dirección IP está disponible), se ejecuta el siguiente bloque de código:

   - Se crea un archivo de configuración de red en `/etc/network/interfaces`. El contenido del archivo se sobrescribe con el siguiente contenido:
     ```
     source /etc/network/interfaces.d/*
     auto lo
     iface lo inet loopback
     
     allow-hotplug enp0s3
     iface enp0s3 inet static
     address $ip_to_assing
     netmask $netmask
     gateway $gateway
     ```
     Este archivo se utiliza para configurar la interfaz de red `enp0s3` con una dirección IP estática.

   - Luego, se reinicia el servicio de red (`networking.service`) utilizando `systemctl restart networking.service`.

   - Finalmente, el script sale del bucle y termina.

En resumen, este script intenta configurar la interfaz de red de la máquina con una dirección IP estática utilizando las direcciones IP del rango definido en `host_id_pool`. Si una dirección IP está disponible (según la comprobación de ping), la configura y reinicia el servicio de red. Si todas las direcciones IP en el rango están en uso, el script termina sin realizar ninguna configuración.



### Implementacion

Para ejecutar el script `ip_assignment.sh` al inicio del sistema utilizando `cron`, puedes seguir estos pasos:

1. Abre el archivo `crontab` del usuario root para editar las tareas programadas. Puedes hacerlo con el siguiente comando:

   ```
   sudo crontab -e
   ```

2. Agrega la línea siguiente al archivo `crontab` para ejecutar el script al inicio del sistema:

   ```
   @reboot /ruta/a/ip_assignment.sh
   ```

   Esto programará la ejecución del script `ip_assignment.sh` utilizando `/bin/bash` al reiniciar el sistema.

3. Guarda y cierra el archivo.

4. El script se ejecutará automáticamente cada vez que reinicies el sistema.

Asegúrate de que el script `/ruta/a/ip_assignment.sh` tenga los permisos de ejecución necesarios. Puedes otorgar estos permisos con el siguiente comando:

```
chmod +x /ruta/a/ip_assignment.sh
```

Esto asegurará que el script se pueda ejecutar correctamente.



## Licencia

```
/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * ALgrHgs wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return. ALgrHgs
 * ----------------------------------------------------------------------------
 */
```

