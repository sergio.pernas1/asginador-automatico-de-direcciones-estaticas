#!/bin/bash


# establecer el pool de direcciones
host_id_pool=(100 101 102)

# establecer la parte de red la direccion ip completa 
ip_network=192.168.0

# establecer la mascara de red
netmask=255.255.255.0

# establecer el gateway de red
gateway=192.168.0.1

# Creamos un bucle que recorre la lista, la variable 'host_id' asumira
# el valor de cada elemento de la lista en cada vuelta.

for host_id in "${host_id_pool[@]}";do

	# En cada vuelta la variable 'ip_to_asign' guarda la direccion IP completa (ip_network + host_id)

	ip_to_assing="${ip_network}.${host_id}"

	# en cada vuelta se hace un ping a la direccion IP que es
	# el valor que tenga '$ip_to_assing'.
	
	if ! ping -c 1 $ip_to_assing > /dev/null 2>&1; then
		
		# Si el ping no responde entonces significa que la ip esta libre
		# y se puede asignar al host, para ello se sobre escribe el fichero
		# de configuraicon '/etc/network/interfaces' (Debian).

		echo "source /etc/network/interfaces.d/*
auto lo
iface lo inet loopback

allow-hotplug enp0s3
iface enp0s3 inet static
	address $ip_to_assing 
	netmask	$netmask
	gateway	$gateway" > /etc/network/interfaces


	# se reinica el servicio de red del sistema operativo para aplicar los cambios.

	systemctl restart networking.service

	# una vez hecho se sale del programa.
	
	exit
	
	fi	


done

